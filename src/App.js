// App.js
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation"

import Accueil from './screens/Accueil.js';
import Connexion from './screens/Connexion.js';
import ForgottedPassword from './screens/ForgottedPassword.js';
import Inscription_mail from './screens/Inscription_mail.js';
import Inscription_password from './screens/Inscription_password.js';
import List from './screens/List.js';


export default class App extends React.Component {
    render() {
        return <AppContainer />;
    }
}

const AppNavigator = createStackNavigator({
    Accueil: {
        screen: Accueil
    },
    Connexion: {
        screen: Connexion
    },
    ForgottedPassword: {
        screen: ForgottedPassword
    },
    Inscription_mail: {
        screen: Inscription_mail
    },
    Inscription_password: {
        screen: Inscription_password
    },
    List: {
        screen: List
    }
}, {
    initialRouteName: "Accueil"
});

const AppContainer = createAppContainer(AppNavigator);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
console.log(styles.container)