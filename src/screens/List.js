import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Button,
    TouchableOpacity,
} from "react-native";

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            histoire: false,
            geographie: false,
            technlogie: false,
            numerique: false,
            list: [],
        };
    }
    render() {
        const removeElement = (array, elem) => {
            var index = array.indexOf(elem);
            console.log(index)
            if (index > -1) {
                array.splice(index, 1);
            }
            return array
        }
        const onPressValidate = () => console.log("Indev : Validate");

        return (
            <View style={styles.container} >
                <Text style={styles.text}>Quels sont vos centre d&lsquo;intérêt ? </Text>
                <View style={styles.Button}>
                    <Button
                        title="Histoire"
                        onPress={() => { this.state.histoire ? this.setState({ list: removeElement(this.state.list, "Histoire") }) : this.setState({ list: this.state.list.concat(["Histoire"]) }); this.setState({ histoire: !this.state.histoire }); }}
                        color={this.state.histoire ? "#00ff00" : "#ff0000"}
                        style={styles.Button}
                    />
                </View >
                <View style={styles.Button}>
                    <Button
                        title="Géographie"
                        onPress={() => { this.state.geographie ? this.setState({ list: removeElement(this.state.list, "Géographie") }) : this.setState({ list: this.state.list.concat(["Géographie"]) }); this.setState({ geographie: !this.state.geographie }); }}
                        color={this.state.geographie ? "#00ff00" : "#ff0000"}
                        style={styles.Button}
                    />
                </View >
                <View style={styles.Button}>
                    <Button
                        title="Technologie"
                        onPress={() => { this.state.technlogie ? this.setState({ list: removeElement(this.state.list, "Technologie") }) : this.setState({ list: this.state.list.concat(["Technologie"]) }); this.setState({ technlogie: !this.state.technlogie }); }}
                        color={this.state.technlogie ? "#00ff00" : "#ff0000"}
                        style={styles.Button}
                    />
                </View >
                <View style={styles.Button}>
                    <Button
                        title="Numérique"
                        onPress={() => { this.state.numerique ? this.setState({ list: removeElement(this.state.list, "Numérique") }) : this.setState({ list: this.state.list.concat(["Numérique"]) }); this.setState({ numerique: !this.state.numerique }); }}
                        color={this.state.numerique ? "#00ff00" : "#ff0000"}
                        style={styles.Button}
                    />
                </View >
                <Text>
                    Liste : {this.state.list.toString()}
                </Text>
                <TouchableOpacity style={styles.validateButton} onPress={onPressValidate}>
                    <Text style={styles.loginText}>Valider</Text>
                </TouchableOpacity>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },

    image: {
        marginBottom: 40,
    },

    text: {

    },


    image_name: {
        width: "75%",
        resizeMode: "stretch",
        marginBottom: 5
    },
    inputView: {
        backgroundColor: "#FFDED7",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,
        alignItems: "center",
    },

    forgot_button: {
        height: 30,
        marginBottom: 30,
    },

    Button: {
        marginBottom: 10,
    },
    button_temp: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#ff0000",
    },

    buttonOn: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#00FF00",
    },

    buttonOff: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#FF0000",
    },
    checkboxContainer: {
        flexDirection: "row",
        marginBottom: 20,
    },
    checkbox: {
        alignSelf: "center",
    },
    label: {
        margin: 8,
    },
    validateButton: {
        width: "50%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#FF6B4C",
    },
});