import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
} from "react-native";


export default class Accueil extends Component {
    render() {
        const onPressConnexion = () => { this.props.navigation.navigate('Connexion'); console.log("Indev : Connexion") };
        const onPressInscription = () => { this.props.navigation.navigate('Inscription_mail'); console.log("Indev : Inscription Mail") };
        const onPressOffline = () => { this.props.navigation.navigate('List'); console.log("Indev : Offline") };
        return (
            <View style={styles.container} >
                <Image style={styles.image_name} source={require("./assets/logoLargeBisFinal.png")} />

                <TouchableOpacity style={styles.Button} onPress={onPressConnexion}>
                    <Text style={styles.loginText}>Connexion</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.Button} onPress={onPressInscription}>
                    <Text style={styles.loginText}>Inscription</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.Button} onPress={onPressOffline}>
                    <Text style={styles.loginText}>Mode Hors ligne</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },

    image: {
        marginBottom: 40,
    },

    image_name: {
        width: "75%",
        resizeMode: "stretch",
        marginBottom: 5
    },
    inputView: {
        backgroundColor: "#FFDED7",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,

        alignItems: "center",
    },

    forgot_button: {
        height: 30,
        marginBottom: 30,
    },

    Button: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#FF6B4C",
    },
});