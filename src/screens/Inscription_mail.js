import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    //Button,
    TouchableOpacity,
} from "react-native";

export default class Inscription_mail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
        };
    }

    render() {
        const onPressInscription_mail = () => { this.props.navigation.navigate('Inscription_password'); console.log("Indev : Register Email") };
        console.log(this.state.email);
        return (
            <View style={styles.container} >

                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Email."
                        placeholderTextColor="#000000"
                        onChangeText={(email) => this.setState({ email: email })}
                    />
                </View>

                <TouchableOpacity style={styles.loginBtn} onPress={onPressInscription_mail}>
                    <Text>Suivant</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },

    image: {
        marginBottom: 40,
    },

    inputView: {
        backgroundColor: "#FFDED7",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,

        alignItems: "center",
    },

    TextInput: {
        height: 50,
        flex: 1,
        padding: 10,
        marginLeft: 20,
    },

    forgot_button: {
        height: 30,
        marginBottom: 30,
    },

    loginBtn: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10,
        backgroundColor: "#FF6B4C",
    },
});