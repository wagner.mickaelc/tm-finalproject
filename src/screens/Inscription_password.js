import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    //Button,
    TouchableOpacity,
} from "react-native";

export default class Inscription_password extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password_1: "",
            password_2: "",
        };
    }

    render() {
        const onPressCreateAccount = () => { this.props.navigation.navigate('Accueil'); console.log("Indev : Create Account") };
        console.log(this.state.password);
        return (
            <View style={styles.container} >

                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Password"
                        placeholderTextColor="#000000"
                        secureTextEntry={true}
                        onChangeText={(password) => this.setState({ password_1: password })}
                    />
                </View>

                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Confirm Password"
                        placeholderTextColor="#000000"
                        secureTextEntry={true}
                        onChangeText={(password) => this.setState({ password_2: password })}
                    />
                </View>

                <TouchableOpacity style={styles.loginBtn} onPress={onPressCreateAccount}>
                    <Text style={styles.loginText}>Créer Compte</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },

    image: {
        marginBottom: 40,
    },

    inputView: {
        backgroundColor: "#FFDED7",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,

        alignItems: "center",
    },

    TextInput: {
        height: 50,
        flex: 1,
        padding: 10,
        marginLeft: 20,
    },

    forgot_button: {
        height: 30,
        marginBottom: 30,
    },

    loginBtn: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10,
        backgroundColor: "#FF6B4C",
    },
});