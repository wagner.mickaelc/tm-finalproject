/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 */

import React from 'react';
// import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Button,
  Share,
  Platform,
} from 'react-native';

const Section = ({ children, title }) => {
  return (
    <View style={styles.sectionContainer}>
      <Text style={[styles.sectionTitle]}>{title}</Text>
      <Text style={[styles.sectionDescription]}>{children}</Text>
    </View>
  );
};

const App = () => {
  return (
    <SafeAreaView>
      <StatusBar barStyle="light-content" />
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View>
          {(Platform.OS == "android" || Platform.OS == "ios") && <>
            <Button
              onPress={() => {
                Share.share({ message: 'Hello world' });
              }}
              title="Partager"
              color="#841584"
              accessibilityLabel="Learn more about this purple button"
            />
          </>}
          <Section title="Step One">
            Editez <Text style={styles.highlight}>App.js</Text> pour
            changer cet écran et revenez voir vos modifications.
          </Section>
          <Section title="See Your Changes">Voici vos changement !</Section>
          <Section title="Debug">Bonjour le monde ! Toujours mieux qu&apos;un Hello World ;)</Section>
          <Section title="Learn More">
            Lisez la documentation pour découvrir quoi faire après !
          </Section>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
